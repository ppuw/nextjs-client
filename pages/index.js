import NavBar from '../components/navbar'
import Footer from '../components/footer'
import SearchBar from '../components/searchbar'
import Cards3Col from '../components/cards3col'
import Gallery from '../components/gallery'

export default function Home() {
  return (
    <>
      <NavBar />
      
      <div className="banner-section h-96 bg-gray-200 flex justify-center items-center">
        <h1 className="text-4xl">Plan and Relax</h1>
      </div>

      <SearchBar />

      <Cards3Col />

      <Gallery />

      <Footer />
    </>
  )
}
