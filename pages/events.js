import NavBar from "../components/navbar";
import Breadcrumbs from '../components/breadcrumbs'
import EventDetails from '../components/eventdetails'
import TabsContainer from '../components/tabs'


export default function Events() {
    return (
        <>
            <style jsx global>{`
                .custom-navbar {
                    box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.13), 0px 1px 8px rgba(0, 0, 0, 0.15);
                }

                .events-container{
                    position: absolute;
                    top: 64px;

                    width: 100%;

                    padding: 30px 0;
                }

                .events-components {

                    margin: auto;
                    max-width: 1400px;

                }
            `}</style>
            

            <NavBar className="custom-navbar" />

            <div className="events-container">
                <div className="events-components">
                    <Breadcrumbs />

                    <EventDetails />

                    <TabsContainer />
                </div>
            </div>

        </>
    )
}