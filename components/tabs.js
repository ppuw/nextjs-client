import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import 'react-tabs/style/react-tabs.css';

export const TabsContainer = () => {
    return (
        <>
            <style jsx global>{`

                .react-tabs__tab-list {
                    border-bottom: none;
                }

                .react-tabs__tab {
                    padding: 10px 26px;
                    outline: none;

                    border: 1px solid #AAAAAA;
                    border-radius: 0;
                    color: #747474;

                    font-weight: 500;
                }

                .react-tabs__tab--selected {
                    color: #ffffff;
                    border: 1px solid #F9B315;
                    background: #F9B315;
                }

                .react-tabs__tab-panel {
                    max-width: 760px;
                }

            `}</style>

            <Tabs>
                <TabList>
                    <Tab>Opis</Tab>
                    <Tab>Terminy</Tab>
                    <Tab>Wiek</Tab>
                    <Tab>Lokalizacjia</Tab>
                    <Tab>Programa</Tab>
                    <Tab>Kontakt</Tab>
                    <Tab>Dodatkowe informacje</Tab>
                </TabList>

                <TabPanel>
                    <h2>Zielona Szkoła – Aktywny Junior to program przygotowany specjalnie z myślą o najmłodszych uczestnikach. Zapraszamy grupy przedszkolne oraz wczesnoszkolne. Zajęcia dostosowane są do umiejętności dzieci i pozwalają im się lepiej poznać. Sprawiają też, że nasi mali Goście znakomicie się bawią w gronie rówieśników, a przy okazji rozwijają swoje umiejętności psychoruchowe. Wszystko to w nowoczesnym Ośrodku wypoczynkowym dla dzieci  „Halo Mazury” usytuowanym w centralnej części Welskiego Parku Krajobrazowego utworzonego w celu ochrony walorów przyrodniczych, historycznych oraz kulturowych regionu oraz  ochrony doliny rzeki Wel.
                        <br />
                     Program Zielonej Szkoły realizowany podczas pobytu trzydniowego lub pięciodniowego przy pobycie pięciodniowym dodatkowe atrakcje, m.in. park linowy. Dla minimum 25 uczestników.</h2>
                </TabPanel>

                <TabPanel>
                    <h2>Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis ratione blanditiis fuga cupiditate excepturi, eos, ipsum error voluptatum non ipsam asperiores ab saepe hic repellat animi quaerat dolore aut ducimus. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Porro ad at voluptates molestias corporis! Eligendi non rem doloribus, corporis vitae iusto totam impedit repellendus velit enim, quasi debitis quibusdam minima.</h2>
                </TabPanel>

                <TabPanel>
                    <h2>Lorem ipsum dolor sit amet consectetur, adipisicing elit. A cupiditate eligendi sed vitae. Similique quos consectetur at eligendi! Magnam, blanditiis non voluptates nisi minus porro ullam recusandae labore? Modi, consequuntur.</h2>
                </TabPanel>

                <TabPanel>
                    <h2>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eveniet laudantium, error deserunt reprehenderit sint dicta, quos, quis perspiciatis commodi perferendis eos nulla ea similique tempora alias corrupti omnis veritatis cum.</h2>
                </TabPanel>

                <TabPanel>
                    <h2>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatum perspiciatis quia, pariatur aspernatur natus hic? Necessitatibus ratione laborum sapiente odit dignissimos repellat minima accusantium autem unde, placeat iure culpa labore?</h2>
                </TabPanel>

                <TabPanel>
                    <h2>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Totam animi minus iusto ipsam veniam recusandae obcaecati distinctio sit voluptate temporibus atque doloribus quod tenetur dolorum, est harum esse magnam consectetur.</h2>
                </TabPanel>

                <TabPanel>
                    <h2>Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem praesentium asperiores ea consequatur in, blanditiis accusantium mollitia saepe magnam at totam facere eius reprehenderit, voluptatibus vero quasi optio veniam. Vitae?</h2>
                </TabPanel>
            </Tabs>
        </>
    )
}

export default TabsContainer

