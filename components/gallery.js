import styles from '../styles/Home.module.css'

export const Gallery = () => {
    return (
        <div className={styles.galleryContainer}>
            <h1 className={styles.galleryTitle}>Popularne wydarzenia</h1>
            <div className={styles.galleryCards}>
                <div className={styles.galleryCard}></div>
                <div className={styles.galleryCard}></div>
                <div className={styles.galleryCard}></div>
                <div className={styles.galleryCard}></div>
                <div className={styles.galleryCard}></div>
                <div className={styles.galleryCard}></div>
                <div className={styles.galleryCard}></div>
                <div className={styles.galleryCard}></div>
            </div>
        </div>
    )
}

export default Gallery