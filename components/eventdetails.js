import iconOnsite from '../images/icon-onsite.svg'
import greenCheck from '../images/green-check.svg'
import star from '../images/star.svg'

export const EventDetails = () => {
    return (
        <>
            <style jsx global>{`
                .event-details-container {
                    display: flex;
                    justify-content: space-between;
                }

                .event-details-left {
                    flex: 50%;
                }

                .event-details-right {
                    flex: 50%;
                }

                img {
                    display: inline;
                }

                .check {
                    margin-right: 16px;
                }

                .rating-container {
                    display: flex;
                }

                .stars {
                    display: flex;
                    justify-content: space-between;
                }

                .star {
                    margin-right: 11px;
                }

                .lightbox-container {
                    width: 100%;
                    max-width: 667px;

                    height: 100%;
                    max-height: 480px;
                }

                ul {
                    list-style-type: none;
                }

                ul li:not(:last-child) {
                    margin-bottom: 11px;
                }
            `}</style>

            <div className="event-details-container mt-14 mb-24">
                <div className="event-details-left">
                    <h3 className="text-gray-400 text-base">Zielona szkoła</h3>
                    <h1 className="text-3xl mb-10 font-medium">Aktywnie na Kaszubach 5 dni</h1>
                    
                    <h2 className="mb-9 text-xl"><span className="font-medium">Pobyt:</span> 2-4 dni</h2>

                    <h2 className="mb-4 text-xl"><span className="font-medium">Cena:</span> od 295 zł do 665 zł</h2>

                    <ul className="mb-12 ml-3">
                        <li><img className="check" src={greenCheck} alt="" /> Wyżywienie</li>
                        <li><img className="check" src={greenCheck} alt="" /> Pokoje 3-4 osobowe</li>
                        <li><img className="check" src={greenCheck} alt="" /> Infrastruktury obiektu</li>
                        <li><img className="check" src={greenCheck} alt="" /> Bezpłatny pobyt dla wychowawcy</li>
                    </ul>

                    <h2 className="mb-14 text-xl"><span className="font-medium">Terminy:</span> kwiecień, maj, czerwiec, wrzesień 2021</h2>

                    <h3 className="mb-2.5 text-gray-400 text-base">Ocena 5 z 5</h3>
                    <div className="rating-container">
                        <div className="stars mr-9">
                            <img className="star" src={star} alt="" />
                            <img className="star" src={star} alt="" />
                            <img className="star" src={star} alt="" />
                            <img className="star" src={star} alt="" />
                            <img className="star" src={star} alt="" />
                        </div>

                        <p className="text-lg text-gray-400">“ Polecam, bardzo polecam. Super ...”</p>
                    </div>

                </div>

                <div className="event-details-right">
                    <h3 className="text-gray-400 text-base mb-5"><img src={iconOnsite} alt="" /> Zobać na mapie</h3>

                    <div className="lightbox-container bg-gray-200 "></div>
                </div>
            </div>
        </>
    )
}

export default EventDetails