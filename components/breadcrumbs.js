import iconHome from '../images/icon-home.svg'

export const Breadcrumbs = () => {
    return (
        <>
            <style jsx global>{`
                .breadcrumbs {
                    display: flex;
                }

                img {
                    display: inline;
                    vertical-align: middle;
                }
            `}</style>

            <div className="breadcrumbs">
                <p className="text-sm"><img src={iconHome} alt=""/> / Zielona szkoła / <span className="text-gray-400">Aktywnie na Kaszubach 5 dni</span></p>
            </div>
        </>
    )
}

export default Breadcrumbs