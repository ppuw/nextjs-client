import styles from '../styles/Home.module.css'

export const SearchBar = () => {
    return (
        <div className={styles.searchbar}>

            <style jsx global>{`

                .checkbox-container {
                    position: absolute;
                    bottom: 20px;
                    left: -40px;

                    display: flex;
                }

                .checkbox {
                    position: relative;
                    margin-left: 120px;
                }
                  
                .checkbox label {
                    background-color: white;
                    border: 1px solid black;
                    border-radius: 50%;
                    cursor: pointer;
                    height: 16px;
                    left: 12px;
                    position: absolute;
                    top: 0;
                    width: 16px;
                }
                  
                .checkbox label:after {
                    border: 2px solid black;
                    border-top: none;
                    border-right: none;
                    content: "";
                    height: 6px;
                    left: 2px;
                    opacity: 0;
                    position: absolute;
                    top: 2px;
                    transform: rotate(-45deg);
                    width: 12px;
                }

                .checkbox label span {
                    position: relative;
                    left: 24px;
                    top: -5px;
                }
                
                .checkbox input[type="checkbox"] {
                    visibility: hidden;
                }
                
                .checkbox input[type="checkbox"]:checked + label {
                    background-color: white;
                    border-color: black;
                }
                
                .checkbox input[type="checkbox"]:checked + label:after {
                    opacity: 1;
                }
            `}</style>

            <div className="checkbox-container">
                <div className="checkbox">
                    <input type="checkbox" id="checkbox-1" />
                    <label for="checkbox-1"><span>Wyżywienie</span></label>
                </div>

                <div className="checkbox">
                    <input type="checkbox" id="checkbox-2" />
                    <label for="checkbox-2"><span>Atrakcje</span></label>
                </div>
            </div>

            <label className={styles.customfield}>
                <select className={styles.dropdown}>
                    <option value="">Szkolne wycieczki</option>
                    <option value="">Szkolne wycieczki</option>
                    <option value="">Szkolne wycieczki</option>
                </select>
                <span className={styles.label}>Wydarzenie</span>
            </label>

            <label className={styles.customfield}>
                <select className={styles.dropdown}>
                    <option value="">Warszawa</option>
                    <option value="">Warszawa</option>
                    <option value="">Warszawa</option>
                </select>
                <span className={styles.label}>Lokalizacja</span>
            </label>

            <label className={styles.customfield}>
                <select className={styles.dropdown2}>
                    <option value="">6-12 lat</option>
                    <option value="">6-12 lat</option>
                    <option value="">6-12 lat</option>
                </select>
                <span className={styles.label}>Wiek</span>
            </label>

            <label className={styles.customfield}>
                <select className={styles.dropdown2}>
                    <option value="">5-7 dni</option>
                    <option value="">5-7 dni</option>
                    <option value="">5-7 dni</option>
                </select>
                <span className={styles.label}>Pobyt</span>
            </label>

            <label className={styles.customfield}>
                <input type="date" className={styles.dropdown} />
                <span className={styles.label}>Termin: od - do</span>
            </label>

            <button className={styles.searchbutton}>Szukaj</button>
        </div>
    )
}

export default SearchBar