import styles from '../styles/Home.module.css'
import iconCertificate from '../images/icon-certificate.svg'
import iconPurchase from '../images/icon-purchase.svg'
import iconChat from '../images/icon-chat.svg'

export const Cards3Col = () => {
    return (
        <div className={styles.cards3colContainer}>
            <div className={styles.cards3colCard}>
                <img className={styles.cards3colIcon} src={iconCertificate} alt=""/>
                <p className={styles.cards3colParagraph}>Wybierasz z szerokiej oferty wydarzeń</p>
            </div>
            <div className={styles.cards3colCard}>
                <img className={styles.cards3colIcon} src={iconPurchase} alt=""/>
                <p className={styles.cards3colParagraph}>Szybko się rejestrujesz  i bezpecznie płacisz</p>
            </div>
            <div className={styles.cards3colCard}>
                <img className={styles.cards3colIcon} src={iconChat} alt=""/>
                <p className={styles.cards3colParagraph}>Bezpośredni kontakt z organizatorem</p>
            </div>
        </div>
    )
}

export default Cards3Col